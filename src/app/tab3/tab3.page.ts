import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AddTaskModel, TODOLIST } from '../Models/todo-list';
import { TodoListService } from '../todo-list.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  doneList: TODOLIST[] = [];

  constructor(public alertController: AlertController, private todoListService: TodoListService) {
  }

  ionViewDidEnter(){
    this.todoListService.getAllTasks().subscribe(tasks =>this.doneList = tasks.filter(task => task.type === 'DONE'));
}

  async presentAlertMultipleButtons(task: TODOLIST) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'DELETE',
      message: `Are you sure you want to delete task: ${task.title} from ${task.type} list ?`,
      buttons: ['Cancel',{
        text: 'Delete',
        role: 'delete',
        cssClass: 'secondary',
        handler: () => {
          this.todoListService.deleteTask(task.id).subscribe(() => this.ionViewDidEnter());
        }
      }]
    });

    await alert.present();
  }

  deleteTask(task: TODOLIST){
    if(task){
      this.presentAlertMultipleButtons(task);
    }
  }

  updateTask(task: TODOLIST, newType: string){
    console.log('update task lunched');
    if(task && newType){
      const taskModified = new AddTaskModel(task.title, task.content, newType);
      this.todoListService.updateTaskType(taskModified, task.id).subscribe(() => this.ionViewDidEnter());
    }
  }

}
